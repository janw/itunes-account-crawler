#!/usr/bin/env python3

import re
import imaplib
import getpass
from email import message_from_bytes
from quopri import decodestring
from os.path import isfile
from datetime import datetime

## Collect credentials and parameters
hostname = input('Hostname: ')
folder   = input('IMAP-Dir: ')
username = input('Username: ')
password = getpass.getpass()
print('iTunes Account: 1) DE 2) US')
language = int(input('Language: '))

# Select patterns based on the selected store language
if language == 1:
    search_patterns = [
       '(SUBJECT "Ihr Beleg"               FROM "do_not_reply@itunes.com" )',
       '(SUBJECT "Ihre Rechnung von Apple" FROM "no_reply@email.apple.com")'
    ]
    date_pattern = r'datum\:\s*([0-9\.]*)'
    date_format = '%d.%m.%Y'
    date_format_alt = '%d.%m.%y'
    date_format_out = '%Y-%m-%d'
    total_pattern = r'gesamt\:\s*([\-0-9\,]*)'
    currency = 'EUR'

elif language == 2:
    raise Exception('US account emails are not yet supported. Help neeed from US citizens!')

    search_patterns = [
       '(SUBJECT "Your Receipt"            FROM "do_not_reply@itunes.com" )',
       '(SUBJECT "Your invoice from Apple" FROM "no_reply@email.apple.com")'
    ]
    date_pattern = r'date\:\s*([\w/0-9\.]*)'
    date_format = '%d.%m.%Y'
    date_format_alt = '%d.%m.%y'
    date_format_out = '%Y-%m-%d'
    total_pattern = r'total\:\s*([\-0-9\,]*)'
    currency = 'USD'

else:
    raise Exception('Currently only German (1) account emails are supported')

## Le jeu commence ...
total = 0
earliest_date = datetime.today()

imap = imaplib.IMAP4_SSL(host=hostname)
imap.login(username, password)
imap.select(folder)

for patterns in search_patterns:

    result, data = imap.search(None, patterns)
    if result != 'OK':
        raise Exception('Search was not successful.', result)

    for message_id in data[0].split():

        result, msg_raw = imap.fetch(message_id, '(RFC822)')
        if result != 'OK':
            raise Exception('Fetch was not successful.', result)

        msg = message_from_bytes(msg_raw[0][1])
        body = ""

        if msg.is_multipart():
            for part in msg.walk():
                ctype = part.get_content_type()
                cdispo = str(part.get('Content-Disposition'))

                if ctype == 'text/plain' and 'attachment' not in cdispo:
                    charset = part.get_content_charset()
                    body = part.get_payload()
                    break
        else:
            charset = msg.get_content_charset()
            body = msg.get_payload()

        ds = decodestring(body).decode(charset)

        res = re.search(date_pattern, ds, flags=re.IGNORECASE)
        try:
            date_of_invoice = datetime.strptime(res.group(1), date_format)
        except ValueError:
            date_of_invoice = datetime.strptime(res.group(1), date_format_alt)

        res = re.search(total_pattern, ds, flags=re.IGNORECASE)
        total += float(res.group(1).replace(',', '.'))
        earliest_date = min(earliest_date, date_of_invoice)

        ## Write all invoices in plaintext to current folder
        filename = date_of_invoice.strftime(date_format_out)+'__'+message_id.decode('utf-8')+'.txt'

        if isfile(filename):
            continue

        with open(filename, 'w') as f:
            print(ds, file=f)

imap.close()
imap.logout()

print('Since {}, You\'ve spent a total amount of {} {:.2f}'.format(earliest_date.strftime(date_format_out), currency, total))